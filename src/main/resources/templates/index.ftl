<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>本地终端</title>
    <link href="/css/xterm.css" rel="stylesheet">
</head>
<body>
<div id="terminal" style="max-width: 98vw;max-height: 95vh"></div>
<script src="/js/xterm.js"></script>
<script src="/js/socket.d.js"></script>
<script src="/js/jquery.min.js"></script>
<script>
    var session, term
    var isInitTerminal = false
    initTerminal();
    /*setTimeout(() => {
        initWs();
    }, 500)*/

    async function initTerminal() {
        term = await new Terminal({
            cursorBlink: true,
            columns: 50,
            rows: 10
        });
        term.open(document.getElementById('terminal'));
        term.onData(command => {
            send(
                action('cmd', command)
            );
        });
        // 监听大小变化
        term.onResize(e => {
            columns = e.cols
            rows = e.rows
            console.log(e)
            if (session)
                send(
                    action('resize', {
                        columns: e.cols,
                        rows: e.rows
                    }));
        })
        initWs();
    }

    async function initWs() {
        //全局单例即可
        session = await SocketD.createClient("sd:ws://" + window.location.hostname + ":${wsPort}/ws/terminal/index?token=123")
            .listen(SocketD.newEventListener()
                .doOnOpen(s => { //会话打开时
                    console.log('open')
                    if (!isInitTerminal) {
                        console.log('web终端已经初始化')
                    }
                    isInitTerminal = true
                    // 全屏
                    term.options.windowOptions.fullscreenWin = true
                    $(window).resize(function () {
                        resizeTerm(term);
                    });
                    // resize 初始化
                    resizeTerm(term);
                    // 行间高度
                    // term.setOption('lineHeight', 1.3)// 设置行高
                    term.options.lineHeight=1.3
                }).doOnMessage((s, m) => { //收到任意消息时
                    if (m.entity().meta('type') === 'print') {
                        term.write(m.entity().dataAsString());
                    }
                })
            )
            .open();
    }

    const send = (str) => {
        const entity = SocketD.newEntity(str);
        session.send("/", entity);
    }

    const action = (type, data) => {
        return JSON.stringify({
            type,
            data,
        });
    }

    function getSize(term) { // 及时调整宽度
        const columns = parseInt(window.innerWidth / 10, 10) - 1
        const rows = parseInt(window.innerHeight / term.options.fontSize, 10) - 7
        return {columns, rows}
    }

    const resizeTerm = (term) => {
        const {columns, rows} = getSize(term)
        if (columns !== term.options.cols || rows !== term.options.rows) {
            term.resize(columns, rows);
            if (session)
                send(
                    action('resize', {
                        columns,
                        rows,
                    })
                );
        }
    };
</script>
</body>
</html>