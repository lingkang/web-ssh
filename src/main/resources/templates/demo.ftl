<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>demo</title>
</head>
<body>
<div id="terminal" style="max-width: 98vw;max-height: 95vh"></div>
<script src="/js/socket.d.js"></script>
<script src="/js/jquery.min.js"></script>
<script>
    var session
    session = SocketD.createClient("sd:ws://" + window.location.hostname + ":9598/?token=123")
        .listen(SocketD.newEventListener()
            .doOnOpen(s => { //会话打开时
                console.log('open')
            }).doOnMessage((s, m) => { //收到任意消息时
                console.log(m.entity().dataAsString())
            })
        )
        .open();
</script>
</body>
</html>