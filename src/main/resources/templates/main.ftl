<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>控制台</title>
    <link href="/res/layui/css/layui.css" rel="stylesheet">
    <link href="/res/css/web-ssh.css" rel="stylesheet">
    <style>
        .layui-tab-content {
            padding: 10px 5px;
            height: calc(95vh - 60px);
            min-height: 250px;
        }

        .layui-tab-item {
            height: 100%;
        }

        .ifr {
            width: 100%;
            height: 100%;
            border: none;
        }

        .layui-icon-close {
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>

<div lay-filter="lay-tab" class="layui-tab layui-tab-brief">
    <ul class="layui-tab-title tab-item">
        <li class="layui-this">控制台</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <iframe src="/view/console" class="ifr"></iframe>
        </div>
    </div>
</div>

<script src="/res/js/jquery.min.js"></script>
<script src="/res/layui/layui.js"></script>
<script>
    var element, index = 1
    layui.use(function () {
        element = layui.element;
        var form = layui.form;
        $('.tab-item').click(function (e) {
            if (e.target) {
                var tag = $(e.target)
                var id = tag.attr('tid');
                if (id === undefined || id === '')
                    return
                var tipCheckbox = window.localStorage.getItem('tipCheckbox')
                if (!tipCheckbox || tipCheckbox !== 'true') {
                    layer.confirm(
                        '<div class="flex-column"><p>确认要关闭此终端吗？</p>' +
                        '<div class="flex-row" style="align-items: center">' +
                        '<input type="checkbox" id="tipCheckbox" style="margin-right: 3px"> 不再提示</div>' +
                        '</div>', {
                            title: '提示',
                            btn: ['确定'] //按钮
                        }, function (index, layero, that) {
                            element.tabDelete('lay-tab', id);
                            layer.close(index);
                            var isChecked = $('#tipCheckbox').prop('checked')
                            if (isChecked)
                                window.localStorage.setItem('tipCheckbox', 'true')
                        });
                } else {
                    element.tabDelete('lay-tab', id);
                    layer.close(index);
                }

            }
        })
    });

    function createTerminal(type) {
        element.tabAdd('lay-tab', {
            title: '<b>本地终端 - ' + index + '</b>' + ' <i class="layui-icon layui-icon-close" tid="' + index + '"></i>',
            content: `<iframe src="/view/terminal/` + index + `?type=` + type + `" class="ifr" tid="` + index + `"></iframe>`,
            id: index,
            change: true // 是否添加完毕后即自动切换
        })
        index++;
    }
</script>
</body>
</html>