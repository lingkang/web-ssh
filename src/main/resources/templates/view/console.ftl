<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>控制台</title>
    <link href="/res/layui/css/layui.css" rel="stylesheet">
    <link href="/res/css/web-ssh.css" rel="stylesheet">
</head>
<body style="padding-top: 20px">


<div class="layui-col-xs8">
    <div class="layui-form flex-column">
        <div class="layui-col-md4" style="margin: 10px 0">
            <button type="button" class="layui-btn layui-btn-fluid createTerminal"> + 新建终端</button>
        </div>
        <div class="layui-col-md4">
            <select id="type">
                <option value="powershell">powershell.exe（仅在window下生效）</option>
                <option value="cmd">cmd.exe（仅在window下生效）</option>
            </select>
        </div>
    </div>
</div>

<script src="/res/js/jquery.min.js"></script>
<script src="/res/layui/layui.js"></script>
<script>
    layui.use(function () {
        var element = layui.element;
        $('.createTerminal').click(function () {
            window.parent.createTerminal($('#type').val())
        })
    });
</script>
</body>
</html>