package top.lingkang.webssh;

import lombok.extern.slf4j.Slf4j;
import org.noear.socketd.SocketD;
import org.noear.socketd.transport.server.Server;
import org.noear.socketd.utils.RunUtils;
import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.core.event.AppLoadEndEvent;
import top.lingkang.webssh.utils.CommonUtils;
import top.lingkang.webssh.ws.WsSimpleListener;

/**
 * @author lingkang
 * @create by 2024/3/19 17:04
 */
@SolonMain
@Slf4j
public class WebApp {
    public static void main(String[] args) {
        Solon.start(WebApp.class, args, solonApp -> {
            solonApp.enableTransaction(false);
            solonApp.enableWebSocket(true);
            //启用 Sokcet.D 服务
            solonApp.enableSocketD(true);
        });
    }
}
