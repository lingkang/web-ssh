package top.lingkang.webssh.ws;

import org.noear.socketd.transport.core.impl.ConfigDefault;
import org.noear.solon.net.annotation.ServerEndpoint;
import org.noear.solon.net.websocket.socketd.ToSocketdWebSocketListener;

/**
 * 将 WebSocket 协议，转为 SocketD  协议
 *
 * @author lingkang
 * @create by 2024/3/21 9:27
 */
@ServerEndpoint
public class WebSocketToSocketd extends ToSocketdWebSocketListener {
    public WebSocketToSocketd() {
        super(new ConfigDefault(false), new WsSimpleListener());
    }

}
