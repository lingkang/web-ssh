package top.lingkang.webssh.controller;

import cn.hutool.core.lang.Assert;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Param;
import org.noear.solon.core.handle.ModelAndView;

/**
 * @author lingkang
 * @create by 2024/3/19 17:05
 */
@Controller
@Mapping
public class WebController {
    @Inject("${server.port:8080}")
    private Integer port;

    @Mapping("/")
    public Object index() {
        return new ModelAndView("main.ftl");
    }

    @Mapping("/demo")
    public Object demo() {
        return new ModelAndView("demo.ftl");
    }

    @Mapping("/view/console")
    public Object console() {
        return new ModelAndView("view/console.ftl");
    }

    @Mapping("/view/terminal/{id}")
    public Object terminal(@Param("id") String id, String type) {
        Assert.notEmpty(id, "参数id不正确");
        return new ModelAndView("view/terminal.ftl").put("port", port).put("id", id).put("type", type);
    }
}
