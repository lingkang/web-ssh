package demo;

import org.noear.snack.ONode;

/**
 * @author lingkang
 * @create by 2024/3/21 14:28
 */
public class Demo01 {
    public static void main(String[] args) {
        String json="{\"type\":\"init\",\"data\":{\"column\":50}}";
        ONode node = ONode.loadStr(json);
        System.out.println(node.select("$.data.column").getString());


    }
}
