# web-ssh


> 终端的web实现。
> Web Implementation of Terminals

## 介绍
[https://yepyn87rxo.k.topthink.com/@3ep4xjom24/jieshao.html](https://yepyn87rxo.k.topthink.com/@3ep4xjom24/jieshao.html)


## 页面

![](https://lfs.k.topthink.com/lfs/658beb397435b0feb0a80c32fb99c1cafbaaf35cf3e3261146cc9cf98d0b7d58.dat)


## window 下

![](https://lfs.k.topthink.com/lfs/7e2eb76a6f3f352e4e1ad76d0a7084d1010d654b30539785975704489ba9a058.dat)


## linux下

![](https://lfs.k.topthink.com/lfs/468021aecdb32a19d7e2ab8e9e4c840a24aab4ef5e0c12042577731910656523.dat)
